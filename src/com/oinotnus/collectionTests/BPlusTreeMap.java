package com.oinotnus.collectionTests;

/*
 *  Copyright 2018 oinotnus@gmail.com
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import java.util.Arrays;
import java.util.Comparator;

public class BPlusTreeMap<K, V> {

	class Bucket extends Node {

		final Node[] nodes;

		@SuppressWarnings("unchecked")
		public Bucket() {
			nodes = new BPlusTreeMap.Node[BUCKET_SIZE];
		}

		public Bucket(Bucket old, Node newLeaf) {
			this();
			this.father = old.father;
			int oldSize = old.size - COPY_ON_SPLIT_LEN;
			System.arraycopy(old.keys, oldSize - 1, keys, 0, COPY_ON_SPLIT_LEN);
			System.arraycopy(old.nodes, oldSize, nodes, 0, COPY_ON_SPLIT_LEN);
			old.size = oldSize;
			this.size = COPY_ON_SPLIT_LEN;
			// Arrays.fill(old.keys, old.size - 1, old.keys.length, null);
			// Arrays.fill(old.nodes, old.size, old.nodes.length, null);
			old.add(newLeaf, old.size);
			for (int i = 0; i < size; i++) {
				nodes[i].father = this;
			}
			// System.arraycopy(old.keys, COPY_ON_SPLIT_LEN, old.keys, 0, COPY_ON_SPLIT_LEN);
			// System.arraycopy(old.nodes, COPY_ON_SPLIT_LEN, old.nodes, 0, COPY_ON_SPLIT_LEN);
		}

		public void add(Node node, int index) {
			int lenToMove = size - index - 1;
			if (lenToMove > 0) {
				System.arraycopy(keys, index, keys, index + 1, lenToMove);
				System.arraycopy(nodes, index + 1, nodes, index + 2, lenToMove);
			}
			size++;
			K key = node.keys[0];
			keys[index] = key;
			nodes[index + 1] = node;

			if (keys[0].equals(nodes[0].keys[0])) {
				throw new IllegalStateException();
			}

			K[] sorted = keys.clone();
			Arrays.sort(sorted, 0, size - 1, comparator);
			if (!Arrays.equals(keys, sorted)) {
				throw new IllegalStateException();
			}

			if (index == 0 && father != null) {
				index = Arrays.binarySearch(father.keys, 0, father.size - 1, key, comparator);
				father.keys[index] = key;
			}
		}

		@Override
		public void formatTo(StringBuilder buffer) {
			nodes[0].formatTo(buffer);
			for (int i = 1; i < size; i++) {
				buffer.append(", ");
				nodes[i].formatTo(buffer);
			}
		}

		@Override
		public boolean isFull() {
			return size == BUCKET_SIZE;
		}

		@Override
		public int search(K key) {
			return Arrays.binarySearch(keys, 0, size - 1, key, comparator);
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("[");
			builder.append("[");
			nodes[0].formatTo(builder);
			builder.append("]");

			for (int i = 1; i < size; i++) {
				builder.append(", ");
				builder.append(keys[i - 1]);
				builder.append("=[");
				nodes[i].formatTo(builder);
				builder.append("]");
			}
			builder.append("]");

			return builder.toString();
		}

	}

	class Leaf extends Node {

		final V[] values;

		@SuppressWarnings("unchecked")
		public Leaf() {
			values = (V[]) new Object[LEAF_SIZE];
		}

		public Leaf(Leaf old) {
			this();
			this.father = old.father;
			old.size -= COPY_ON_SPLIT_LEN;
			size = COPY_ON_SPLIT_LEN;
			System.arraycopy(old.keys, old.size, keys, 0, COPY_ON_SPLIT_LEN);
			System.arraycopy(old.values, old.size, values, 0, COPY_ON_SPLIT_LEN);
			Arrays.fill(old.keys, old.size, old.keys.length, null);
			Arrays.fill(old.values, old.size, old.values.length, null);
		}

		public void add(K key, V value, int index) {
			if (index < size) {
				System.arraycopy(keys, index, keys, index + 1, size - index);
				System.arraycopy(values, index, values, index + 1, size - index);
			}
			size++;
			keys[index] = key;
			values[index] = value;
			if (index == 0 && father != null) {
				index = Arrays.binarySearch(father.keys, 0, father.size - 1, key, comparator);
				father.keys[index] = key;
			}

		}

		@Override
		public void formatTo(StringBuilder buffer) {
			if (size > 0) {
				buffer.append(keys[0]);
				buffer.append('=');
				buffer.append(values[0]);
			}
			for (int i = 1; i < size; i++) {
				buffer.append(", ");
				buffer.append(keys[i]);
				buffer.append('=');
				buffer.append(values[i]);
			}
		}

		public V set(K key, V value, int index) {
			V old = values[index];
			values[index] = value;
			return old;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("[");
			formatTo(builder);
			builder.append("]");
			return builder.toString();
		}

	}

	abstract class Node {

		final K[] keys;

		int size;
		Bucket father;

		@SuppressWarnings("unchecked")
		public Node() {
			keys = (K[]) new Object[LEAF_SIZE];
		}

		abstract void formatTo(StringBuilder buffer);

		public boolean isFull() {
			return size == LEAF_SIZE;
		}

		public int search(K key) {
			return Arrays.binarySearch(keys, 0, size, key, comparator);
		}

	}

	private static int LEAF_SIZE = 4;

	private static int BUCKET_SIZE = LEAF_SIZE + 1;

	private static int COPY_ON_SPLIT_LEN = LEAF_SIZE / 2;

	private static void doPut(BPlusTreeMap<Integer, Integer> map, int key, int value) {
		System.out.println("Put " + key + " : " + value + " => " + map.put(key, value));
		System.out.println(map);
	}

	public static void main(String[] args) {
		BPlusTreeMap<Integer, Integer> map = new BPlusTreeMap<>();
		doPut(map, 1, 0);
		for (int i = 1; i < 13; i++) {
			doPut(map, i, i);
		}
		for (int i = 13; i < 32; i++) {
			doPut(map, i, i);
		}

	}

	private Node root;

	private Comparator<K> comparator;

	@SuppressWarnings("unchecked")
	public BPlusTreeMap() {
		root = new Leaf();
		comparator = (Comparator<K>) Comparator.naturalOrder();
	}

	private void createRootBucket(Node newLeaf, Node oldLeaf) {
		Bucket root = new Bucket();
		root.size = 2;
		root.keys[0] = newLeaf.keys[0];
		root.nodes[0] = oldLeaf;
		root.nodes[1] = newLeaf;
		oldLeaf.father = root;
		newLeaf.father = root;
		this.root = root;
	}

	public void formatTo(StringBuilder buffer) {
		root.formatTo(buffer);
	}

	public V get(K key) {
		Node node = root;
		while (node instanceof BPlusTreeMap.Bucket) {
			Bucket bucket = (Bucket) node;
			int index = bucket.search(key);
			index = index < 0 ? -index - 1 : index;
			if (index > bucket.size) {
				return null;
			}
			node = bucket.nodes[index];
		}
		Leaf leaf = (Leaf) node;
		int index = Arrays.binarySearch(leaf.keys, key);
		if (index < 0) {
			return null;
		}
		return leaf.values[index];
	}

	private void propagateToBucket(Node newLeaf, Node oldLeaf) {
		Bucket oldBucket = oldLeaf.father;

		if (oldBucket == null) {
			createRootBucket(newLeaf, oldLeaf);
		} else {

			if (oldBucket.isFull()) {
				Bucket newBucket = new Bucket(oldBucket, newLeaf);
				propagateToBucket(newBucket, oldBucket);
			} else {
				int index = oldBucket.search(oldLeaf.keys[0]);
				if (index < 0)
					oldBucket.add(newLeaf, -index - 1);
				else
					oldBucket.add(newLeaf, index + 1);
			}
		}
	}

	public V put(K key, V value) {
		Leaf leaf = searchLeaf(key);
		int index = leaf.search(key);
		if (index >= 0) {
			return leaf.set(key, value, index);
		}
		if (leaf.isFull()) {
			splitAndPut(leaf, key, value, -index - 1);
		} else {
			leaf.add(key, value, -index - 1);
		}
		return null;

	}

	@SuppressWarnings("unchecked")
	private Leaf searchLeaf(K key) {
		Node node = root;
		while (node instanceof BPlusTreeMap.Bucket) {
			Bucket bucket = (Bucket) node;
			int index = bucket.search(key);
			index = index < 0 ? -index - 1 : index;
			if (index > bucket.size) {
				index = bucket.size - 1;
			}
			node = bucket.nodes[index];
		}
		return (Leaf) node;
	}

	private void splitAndPut(Leaf oldLeaf, K key, V value, int newLeafIndex) {
		Leaf newLeaf = new Leaf(oldLeaf);
		int oldLeafIndex = newLeafIndex - COPY_ON_SPLIT_LEN;
		if (oldLeafIndex < 0) {
			oldLeaf.add(key, value, newLeafIndex);
		} else {
			newLeaf.add(key, value, oldLeafIndex);
		}

		propagateToBucket(newLeaf, oldLeaf);
	}

	@Override
	public String toString() {
		StringBuilder buffer = new StringBuilder();
		formatTo(buffer);
		return buffer.toString();
	}

}
